# Test - Le serveur répond à une requête simple en local


- Tester via un navigateur : [index](https://127.0.0.1:8080/index.html)
-> Affiche le texte contenu de index.html, une citation de Son Gohan.

OU

- En ligne de commandes : 
`curl --insecure https://127.0.0.1:8080/index.html`
-> Affiche le texte contenu de index.html, une citation de Son Gohan.
