CPPFLAGS = -DNO_VND
LIBB = -L/usr/local/lib/lib -lboost_url -lboost_filesystem -lcrypto -lssl

main.o : main.cpp vnd-0.5.6/server.cpp vnd-0.5.6/secure/server_certificate.hpp vnd-0.5.6/utils.cpp
	g++ -pthread vnd-0.5.6/utils.cpp vnd-0.5.6/server.cpp main.cpp -I /Bureau/Boost -std=c++11 -Wall -Wextra -o main.o $(LIBB) $(CPPFLAGS)

clean :
	rm -f *.o

run : main.o
	./main.o

