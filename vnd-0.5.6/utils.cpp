#include "utils.hpp"

using namespace std;

int 
Utils::split(string str, char separator, string* strings)  
{
    if (str == "") { return 0; }
    
    int currIndex = 0, i = 0;  
    int startIndex = 0, endIndex = 0;  
    while (i <= boost::numeric_cast<int>(str.length()))  
    {  
        if (str[i] == separator || i == boost::numeric_cast<int>(str.length()))
        {  
            endIndex = i;  
            string subStr = "";  
            subStr.append(str, startIndex, endIndex - startIndex);  
            strings[currIndex] = subStr;  
            currIndex += 1;  
            startIndex = endIndex + 1;  
        }  
        i++;  
    }
    return currIndex;
}

int 
Utils::parseCommand(
    boost::beast::http::request<boost::beast::http::string_body> const& req,
    string* params,
    string* command)
{
    string str = string(req.target());
    split(str, '?', params);
    command[0] = params[0].substr(1, params[0].length()); // We cut off the '/' char at the beginning

    int params_cpt = split(params[1], '&', params);
    string tmp_strings[2];
    for ( int i = 0; i < params_cpt; i++ ) {
        split(params[i], '=', tmp_strings);
        params[i] = tmp_strings[1];        
    }

    return params_cpt;
}
