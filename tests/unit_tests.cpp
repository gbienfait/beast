#define BOOST_TEST_MODULE Unit Tests

#include <boost/test/included/unit_test.hpp>
#include "../vnd-0.5.6/utils.hpp"

#define STRINGS_BUFF_LARGE_ENOUGH 32

BOOST_AUTO_TEST_CASE(utils_unit_tests)
{
  std::string long_str = "Tu as peut-être envahi mon esprit et mon corps, mais un Saiyen garde toujours sa fierté";
  int const long_str_words_cpt = 16;
  int const long_str_words_cpt_comma = 2;
  std::string str = "Cell";
  std::string empty_str = "";
  std::string command_str = "add?arg1=eth0&arg2=enbas";
  std::string strings[STRINGS_BUFF_LARGE_ENOUGH];

  // TEST of Utils::split()
  int words_cpt = Utils::split(long_str, ' ', strings);
  BOOST_TEST(words_cpt);
  BOOST_TEST(strings);
  BOOST_TEST(strings[1] == "as");
  BOOST_TEST(words_cpt == long_str_words_cpt);

  words_cpt = Utils::split(long_str, ',', strings);
  BOOST_TEST(words_cpt);
  BOOST_TEST(strings);
  BOOST_TEST(words_cpt == long_str_words_cpt_comma);

  words_cpt = Utils::split(empty_str, ',', strings);
  BOOST_TEST(words_cpt == 0);
}
