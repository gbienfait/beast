# Test - Le serveur répond une commande textuelle à partir d'une commande REST
 
`curl --insecure https://127.0.0.1:8080/add?arg1=eth0\&arg2=nic`
-> Affiche "add eth0 nic"
