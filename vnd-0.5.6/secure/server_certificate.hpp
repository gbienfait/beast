//
// Copyright (c) 2016-2019 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//

#ifndef BOOST_BEAST_EXAMPLE_COMMON_SERVER_CERTIFICATE_HPP
#define BOOST_BEAST_EXAMPLE_COMMON_SERVER_CERTIFICATE_HPP

#include <boost/asio/buffer.hpp>
#include <boost/asio/ssl/context.hpp>
#include <cstddef>
#include <memory>

/*  Load a signed certificate into the ssl context, and configure
    the context for use with a server.

    For this to work with the browser or operating system, it is
    necessary to import the "Beast Test CA" certificate into
    the local certificate store, browser, or operating system
    depending on your environment Please see the documentation
    accompanying the Beast certificate for more details.
*/
inline
void
load_server_certificate(boost::asio::ssl::context& ctx)
{
    /*
        The certificate was generated from bash on Ubuntu (OpenSSL 1.1.1f) using:

        openssl dhparam -out dh.pem 2048
        openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 10000 -out cert.pem -subj "/C=US/ST=CA/L=Los Angeles/O=Beast/CN=www.example.com"
    */

    std::string const cert =
        "-----BEGIN CERTIFICATE-----\n"
        "MIIDizCCAnOgAwIBAgIUPAA8hD1gQtNRH0Vnsu4O3RhEb0EwDQYJKoZIhvcNAQEL\n"
        "BQAwVDELMAkGA1UEBhMCVVMxCzAJBgNVBAgMAkNBMRQwEgYDVQQHDAtMb3MgQW5n\n"
        "ZWxlczEOMAwGA1UECgwFQmVhc3QxEjAQBgNVBAMMCTEyNy4wLjAuMTAgFw0yMzAy\n"
        "MTcxMjU1MDVaGA8yMDUwMDcwNTEyNTUwNVowVDELMAkGA1UEBhMCVVMxCzAJBgNV\n"
        "BAgMAkNBMRQwEgYDVQQHDAtMb3MgQW5nZWxlczEOMAwGA1UECgwFQmVhc3QxEjAQ\n"
        "BgNVBAMMCTEyNy4wLjAuMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB\n"
        "AO3CpQRm0AtduT6521e5glVeg2uAeLxp1nCGoF0LYL0pyTUI7AfRuO2RcgWUfMtN\n"
        "b7C4BgabUJUxRb70EBKJ7qi8wT+05uFPs1GUYKQpbnV+cvFIsXY9lK9rYhX3WYR3\n"
        "yEEq9wqa6P2ikcEw6AosGHmpTQ1Vn1Zao4ayD5bdZJ0BvLeoT98POwEJPRZIMUWn\n"
        "b/zC7co87AhX6ydywuFzGNhqHYqO1FsmauROT0LrBJ6JBwuZ092OeAJOUNWfte0e\n"
        "o0BHM7IDcz+yKs071zjUXqLcyCVzIVD6TQvk8zNzekNkoohJrLBB7LeClAVHgWer\n"
        "e5/N6i2oibFtbNCx6fRAWAMCAwEAAaNTMFEwHQYDVR0OBBYEFKIugqfnB+kuKkQY\n"
        "k5l/hWGAXrKIMB8GA1UdIwQYMBaAFKIugqfnB+kuKkQYk5l/hWGAXrKIMA8GA1Ud\n"
        "EwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAOCJzZkNa2OMswP9XbXfvdV3\n"
        "TssSRz6x6mfNWFOBBKW3/b++iwKQOwRGtqn3kF8vbn2rD88Ulax62O1JV/xManBn\n"
        "W+soOPfuCekxyHbh0c6/DEC6RCGyTg4XSxKUvM+tMVwZI2u9YSvfT/Fyz32bWg13\n"
        "Z0hSOpGms55H32txzGDxkE+D00qe5uHC3SZIZAqYvru8cBOq8KvQxPE7iQVF5179\n"
        "58JYIuzAVopF1ZFBJ9GSRydyyNrzF3x5LZkCkp+T6boV8izGHyGnWJFTrg/F01xA\n"
        "L9Kd/uTrqZYurLR23OgKRfJ1SntAUvCE14lvNOml1QoPiKRu/MnOiF+VWyxMBGE=\n"
        "-----END CERTIFICATE-----\n";

    std::string const key =
        "-----BEGIN PRIVATE KEY-----\n"
        "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDtwqUEZtALXbk+\n"
        "udtXuYJVXoNrgHi8adZwhqBdC2C9Kck1COwH0bjtkXIFlHzLTW+wuAYGm1CVMUW+\n"
        "9BASie6ovME/tObhT7NRlGCkKW51fnLxSLF2PZSva2IV91mEd8hBKvcKmuj9opHB\n"
        "MOgKLBh5qU0NVZ9WWqOGsg+W3WSdAby3qE/fDzsBCT0WSDFFp2/8wu3KPOwIV+sn\n"
        "csLhcxjYah2KjtRbJmrkTk9C6wSeiQcLmdPdjngCTlDVn7XtHqNARzOyA3M/sirN\n"
        "O9c41F6i3MglcyFQ+k0L5PMzc3pDZKKISaywQey3gpQFR4Fnq3ufzeotqImxbWzQ\n"
        "sen0QFgDAgMBAAECggEBAMfXe/Y4KJP1IhgcBcY1cdwphdLc+B594zVPowix4Iz+\n"
        "y1Ude66S05ozTyBrj30eSEvEUaRJ9bCA6ru2zq+WahhAFzph5lHpOLLQQW6aOHLf\n"
        "ZuNxk1/sIMZVPutxrTdPqu1DnMO4My/E7jfw0IiEScowyEf1+OiPZLV5guruiXwA\n"
        "9AflJvPfnBBpP0E/XkqDu8judjyJDyARtbjjucVsQwBOVgxvzl3Qn8ErPERM8ySB\n"
        "aNA3bpd4/606rGm7kjxj3Knp0/lIHt519KPydRGnG5jVCTmER2DvP3f2rV6pkMd+\n"
        "ugC4yT02uNRI185qXrhMchLkBLT4RP0B0oKmMAPPA9kCgYEA+YYK59LxGEUqGxEv\n"
        "e6r/nb+r+iWbQqPf58/8xjXUonH4Zfu5hF2J//e1hs9O8WjlKxJN4bmFcIoQgNfP\n"
        "Sam2+CJKKzVuzXWqg5oHTRuzKcABV/5fLbQ2aRj+sxn67eCXcvpJqaS8gnXVRZQQ\n"
        "F+l+7yxSKYhN39PxNxdIRE6OFBUCgYEA8+5w52wRt9zud1Ho7pcz04lr5Avq8pG5\n"
        "wD/gyiTAms4S/ilnvOOyhcSx0PtOmMb72djY/oWTdtHH063493YX4drWHKydiF0w\n"
        "gMMbETZNvIie/CSBb0cXwT74r1eDdCUfvm0ubN/wOHb0tBQxqtTpYOm9Ymuxxh6v\n"
        "Fje0yFSeSbcCgYAxVf2GBMFCyTogC+/VhePwrts0JIdkT76PuSIDltUqIVq532G4\n"
        "+D227KVpuEell9XoBEB95w6pR8MnloQXzoSs3yrHDFOSMDe8uZHD/zwXfy1E1ri+\n"
        "8qjncte5VeO1cPjA9RfngPj82ouGrFXaRIKii6LbL2YmYWOKPwI870NMWQKBgQDC\n"
        "sFWWr494szH1M//4pd+sf2sISWbNEfgydAli7XB4Xe+UwdFVVlq/H11Y0atKjs6m\n"
        "ccfHZS9sT0TxM1KZl7y5++KWYkMdelti+dXZqADlcBmMX0qbWR+hkjQEJpH5mkO6\n"
        "xgOezCE7v93Q+KknkuVt/0CzlAcAfK77yZxVpAvuAwKBgQC23zP22Sf3GyaDojGD\n"
        "GIwMd9KKIVvMnGT/vLZxZDMYAipjx8SJA7Db+x6apgo6/cApXXGKpDHBxQ6xtNHc\n"
        "KQcn+oO119PEl59R/lg5ikAWT/zGv5Apn8ABtnZjgdTZg/l6SI3UsnUgdLn0GxMB\n"
        "SCDASFt7F7a7wu1W0PKsZAV74Q==\n"
        "-----END PRIVATE KEY-----\n";

    std::string const dh =
        "-----BEGIN DH PARAMETERS-----\n"
        "MIIBCAKCAQEAuFvaZU4SZ6P5LxKqZaBtRrB4zS/fpP3RRf4gllRi1l2yHbBwkoij\n"
        "Fnr5jQuiovXVTn/CUwL1gnpkvwQt+uD3DGqxM/hjBypSsgW4YBfzs5rnE+bmnEBF\n"
        "1Jfz6j6x9ig2eX/AWkWp/vCmdfdKfxp2LfL5W+d4OwYsVny/0Hzgk/mEuvS0FKAY\n"
        "Wu2np/LvRi/q4pvCA68bAtnVZoKIaff1d9SJbsSbM3peglAK7bQQDZXcAQr6OzR7\n"
        "fctiq1UvEZYMrXzRaGNs9BR5I35w6tffQidrVpxgEqTpm1NKONGENmT7aT0sls32\n"
        "Qu48rDazSKIxCfRMb8jAhGC4jG81BUrqKwIBAg==\n"
        "-----END DH PARAMETERS-----\n";
    
    ctx.set_password_callback(
        [](std::size_t,
            boost::asio::ssl::context_base::password_purpose)
        {
            return "test";
        });

    ctx.set_options(
        boost::asio::ssl::context::default_workarounds |
        boost::asio::ssl::context::no_sslv2 |
        boost::asio::ssl::context::single_dh_use);

    ctx.use_certificate_chain(
        boost::asio::buffer(cert.data(), cert.size()));

    ctx.use_private_key(
        boost::asio::buffer(key.data(), key.size()),
        boost::asio::ssl::context::file_format::pem);

    ctx.use_tmp_dh(
        boost::asio::buffer(dh.data(), dh.size()));
}

#endif