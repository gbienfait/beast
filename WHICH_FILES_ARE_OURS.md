# Quels fichiers avons-nous nous-mêmes produits ?
Ce fichier servira à renseigner les évaluateurs du projet sur les fichiers qui sont ou pas de notre production au sein du projet.
Les fichiers et dossiers sont indiqués par un chemin relatif depuis la racine du projet.

## De notre production
Produits dans le cadre du projet par nous-mêmes.
- .gitignore
- Makefile
- README.md
- main.cpp
- page_test.html
- docker-beast/*
- tests/*
- vnd-0.5.6/secure/*
- vnd-0.5.6/index.html
- vnd-0.5.6/server.cpp
- vnd-0.5.6/server.hpp
- vnd-0.5.6/utils.cpp
- vnd-0.5.6/utils.hpp


## Les fichiers récupérés de VND
Produit par M. Magoni dans le cadre du projet VND.
- vnd-0.5.6/endpoint.cpp
- vnd-0.5.6/endpoint.hpp
- vnd-0.5.6/iface.cpp
- vnd-0.5.6/iface.hpp
- vnd-0.5.6/main.cpp
- vnd-0.5.6/Makefile
- vnd-0.5.6/medeb.cpp
- vnd-0.5.6/medeb.hpp
- vnd-0.5.6/mesap.cpp
- vnd-0.5.6/mesap.hpp
- vnd-0.5.6/mt19937.cpp
- vnd-0.5.6/mt19937.hpp
- vnd-0.5.6/prost.cpp
- vnd-0.5.6/prost.hpp
- vnd-0.5.6/util.cpp
- vnd-0.5.6/util.hpp
- vnd-0.5.6/vnd.cpp
- vnd-0.5.6/vnd.hpp
- vnd-0.5.6/vnd.txt


## Les fichiers de VND modifiés dans le cadre du projet

- vnd-0.5.6/vnd.hpp :
	Ajout d'un objet de type Server (ligne 45 et ligne 50)
- vnd-0.5.6/main.cpp :
	Ajout du lancement du serveur dans un thread (lignes 69-76)
