# Beast dans un container
Comment exécuter beast en utilisant un container Docker.

## Pré-requis
Installer [Docker Desktop](https://docs.docker.com/desktop/install/linux-install/).

## Étape - Via Docker hub
Cette méthode implique de télécharger une image conséquente (2Go).
1. Récupérer et lancer l'image avec `docker run -p 8080:8080 guillermosalvaje/catsonbeast`

### Ou

## Étapes - Via `docker compose`
Cette méthode implique un long temps d'installation (~15-30mn selon votre machine).
1. Cloner le projet : `git clone git@gitlab.emi.u-bordeaux.fr:gbienfait/beast.git`

2. `cd beast/docker-beast`

3. Créer et lancer le container : `docker compose up`
