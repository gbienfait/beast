#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/strand.hpp>
#include <boost/make_unique.hpp>
#include <boost/optional.hpp>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>
#include <fstream>

namespace beast = boost::beast;                 // from <boost/beast.hpp>
namespace http = beast::http;                   // from <boost/beast/http.hpp>
using namespace std;

class Utils {
    public:

        /**
         * @brief Evaluates the length of a string.
         * 
         * @param str
         * @return The length of str
         */
        static int len(string str);

        /**
         * @brief Splits a string into multiple strings.
         * 
         * @param str The initial string
         * @param separator
         * @param strings The array of strings that gets the result
         * @return The number of words in strings
         */
        static int split(string str, char separator, string* strings);

        /**
         * @brief Parse a VND command gave via GET method in HTTP request.
         * 
         * @param req The HTTP request
         * @param params The array of string that gets the parameters of the command
         * @param command The string that gets the command
         * @return The number of parameters (i.e. the number of words in params)
         */
        static int parseCommand(
            boost::beast::http::request<boost::beast::http::string_body> const& req,
            string* params,
            string* command);
};
