# Beast

## Fichiers produits dans le cadre du projet
Voir [WHICH_FILES_ARE_OURS.md](WHICH_FILES_ARE_OURS.md).

## Lancer le projet via un container Docker
Voir [docker-beast/README.md](docker-beast/README.md).


## Pré-requis
### Installer Boost

Installation dérivée de [Boost Getting Started](https://www.boost.org/doc/libs/1_81_0/more/getting_started/unix-variants.html).

1. Télécharger [boost_1_81_0.tar.bz2](https://www.boost.org/users/history/version_1_81_0.html)

2. Dé-zipper : `tar --bzip2 -xf /path/to/boost_1_81_0.tar.bz2`

3. Accéder au dossier `cd path/to/boost_1_81_0`

4. Préparer l'installation avec `sudo ./bootstrap.sh`

5. Installer avec `sudo ./b2 install`

6. Mettre à jour le chemin du système vers les "libraries" `sudo ldconfig`

## Installation
1. Installer les paquets de développement openssl

`sudo apt-get install libssl-dev`

2. Cloner le projet et accéder au dossier

`git clone git@gitlab.emi.u-bordeaux.fr:gbienfait/beast.git`

`cd beast`

3. (Pas obligatoire avec client en mode insecure) Ajouter le certificat du serveur à ses certificats de confiance

`sudo apt-get install -y ca-certificates`

`sudo cp vnd-0.5.6/secure/certs/local-ca.crt /usr/local/share/ca-certificates`

`sudo update-ca-certificates`

4. Compiler le serveur

`make`

5. Mettre le serveur en écoute en local

`make run`

6. Tester
- via un navigateur (HTTP) : [index](http://127.0.0.1:8080/index.html)
- via un navigateur (HTTPS, nécessite étape 3) : [index](https://127.0.0.1:8080/index.html)
- en ligne de commandes (HTTP) : `curl http://127.0.0.1:8080/index.html`
- en ligne de commandes (HTTPS) : `curl --insecure https://127.0.0.1:8080/index.html`

## Tests unitaires
- Accéder au dossier des tests : `cd beast/tests`
- Lancer les tests unitaires : `make run`
