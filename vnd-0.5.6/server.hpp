//
// Copyright (c) 2016-2019 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//

//------------------------------------------------------------------------------
//
// Example: Advanced server, flex (plain + SSL)
//
//------------------------------------------------------------------------------

#include "secure/server_certificate.hpp"

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/strand.hpp>
#include <boost/make_unique.hpp>
#include <boost/optional.hpp>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include <boost/filesystem/fstream.hpp>
#include <boost/url/url.hpp>
#include <boost/url/parse.hpp>
#include <boost/url/segments_encoded_ref.hpp>
#include <boost/url/segments_encoded_view.hpp>
#include <boost/url/string_view.hpp>
#include <boost/url/url.hpp>
#include <boost/url/url_view.hpp>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include "utils.hpp"

namespace fs = boost::filesystem;
namespace urls = boost::urls;
namespace beast = boost::beast;                 // from <boost/beast.hpp>
namespace http = beast::http;                   // from <boost/beast/http.hpp>
namespace websocket = beast::websocket;         // from <boost/beast/websocket.hpp>
namespace net = boost::asio;                    // from <boost/asio.hpp>
namespace ssl = boost::asio::ssl;               // from <boost/asio/ssl.hpp>
using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>

namespace vnd
{
    
    class network_device;

    class Server
    {
    public:
        network_device * nd;

        Server();

        int run();

        // Return a reasonable mime type based on the extension of a file.
        beast::string_view mime_type(beast::string_view path);

        // Append an HTTP rel-path to a local filesystem path.
        // The returned path is normalized for the platform.
        std::string path_cat(beast::string_view base, beast::string_view path);


        // Return a response for the given request.
        //
        // The concrete type of the response message (which depends on the
        // request), is type-erased in message_generator.
        template<class Body, class Allocator>
        http::message_generator
        handle_request(
            beast::string_view doc_root,
            http::request<Body, http::basic_fields<Allocator>>&& req);


        // Report a failure
        void
        fail(beast::error_code ec, char const* what);

        template<class Body, class Allocator>
        void
        make_websocket_session(
            beast::tcp_stream stream,
            http::request<Body, http::basic_fields<Allocator>> req);

        template<class Body, class Allocator>
        void
        make_websocket_session(
            beast::ssl_stream<beast::tcp_stream> stream,
            http::request<Body, http::basic_fields<Allocator>> req);
    };

}