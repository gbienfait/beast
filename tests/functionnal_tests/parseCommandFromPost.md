# Test - Le serveur répond une commande textuelle à partir d'une commande REST en POST
 
`curl -X POST -H "Content-Type: application/json" -d '{"command": "add eth0"}' http://127.0.0.1:8080`
-> Affiche "add eth0"
